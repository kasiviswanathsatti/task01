package ultratendency.task01;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;
import org.json.JSONObject;

//import kafka.consumer.ConsumerConfig;
import kafka.serializer.StringDecoder;
import scala.Tuple2;

public class Consumer {
    
	// create the hbase configuration
	static Configuration hconf = HBaseConfiguration.create();

	public static void main(String[] args) throws InterruptedException {
        /* The topics of kafka from which data has to be read are stored in a variable of type ‘Set’. 
         * Parameters for Kafka Connection are stored in a ‘Map’ object. */   
		Map<String, String> kafkaParams = new HashMap<String, String>();
		Set<String> topics = new HashSet<String>(Arrays.asList("topic1", "topic2", "topic3"));
 
		// metadata.broker.list defines where the Consumer can find a one  
		// or more Brokers, to connect to Kafka Cluster.
		kafkaParams.put("metadata.broker.list", "kafka:9092");
        // identifier of the group consumer belongs to.
		kafkaParams.put(ConsumerConfig.GROUP_ID_CONFIG, UUID.randomUUID().toString());
		// The id string to pass to the server when making requests. 
		kafkaParams.put(ConsumerConfig.CLIENT_ID_CONFIG, "Consumer");
		// ‘smallest’, reads the data using Kafka  Consumer api ‘from beginning’. 
		kafkaParams.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "smallest");

		int STREAM_WINDOW_MILLISECONDS = 2000; // 2 seconds
         /* key is of type string, so key serializer defines string serializer,
		    when preparing the message for transmission to the broker.
		*/ 
		SparkConf conf = new SparkConf().setAppName("Kafka Spark Streaming App").setMaster("local[*]");
        // A JavaStreamingContext object can be created from a SparkConf object. 
		JavaStreamingContext ssc = new JavaStreamingContext(conf, new Duration(STREAM_WINDOW_MILLISECONDS));
        /* Integrate kafka with spark streaming using kafkaUtils,
         * provides parameters like Streaming Context, type of data transferred, Kafka Params containing broker hosts, and topic names.
         * The outputs received from this stream of data is a ‘DStream’  object and we call it ‘directKafkaStream’.
		*/
		JavaPairInputDStream<String, String> directKafkaStream = KafkaUtils.createDirectStream(ssc, String.class,
				String.class, StringDecoder.class, StringDecoder.class, kafkaParams, topics);

		directKafkaStream.print();

		/* from DStream extract each RDD and within java lambda function extract partition from each object that we iterate.
		 * each partition contains key value pairs, where value contains the actual data which it reads from the Kafka topic. 
		 */
		directKafkaStream.foreachRDD(rdd -> {
			rdd.foreachPartition(iterator -> {
				int i = 1;
				while (iterator.hasNext()) {
                    // get the value which is actual data.
					Tuple2<String, String> next = iterator.next();

					System.out.println("#####" + next);

					// the actual data is in json format, convert the received data into json format and get the objects.
					JSONObject jobj = new JSONObject(next._2());
					JSONObject dataobj = jobj.getJSONObject("data");
					JSONObject locobj = dataobj.getJSONObject("location");

					// int temp = Integer.parseInt(dataobj.getString("temperature").toString());
					// System.out.println(temp);

					int temp = (int) dataobj.get("temperature");
					System.out.println(temp);
					String temps = String.valueOf(temp);

					Long t = (Long) dataobj.get("time");
					System.out.println(t);
					String times = String.valueOf(t);
                    
					// get zookeeper properties
					hconf.set("hbase.zookeeper.quorum", "localhost");
					hconf.set("hbase.zookeeper.property.clientPort", "2181");
					// connect to hbase and get the table
					Connection connection = ConnectionFactory.createConnection(hconf);
					Table table = connection.getTable(TableName.valueOf("ultratendency"));
                    // To insert data into table addColumn() method of Put class is used.   
					Put p = new Put(Bytes.toBytes("row" + i));

					/*  The method addColumn() accepts the byte array, so get the values from json object 
					 *  Convert every value to byte and put the values into HBase Table. 
					 */

					p.addColumn(Bytes.toBytes("weather-data"), Bytes.toBytes("deviceId"),
							Bytes.toBytes(dataobj.getString("deviceId")));
					p.addColumn(Bytes.toBytes("weather-data"), Bytes.toBytes("temperature"), Bytes.toBytes(temps));
					p.addColumn(Bytes.toBytes("weather-data"), Bytes.toBytes("latitude"),
							Bytes.toBytes(locobj.getString("latitude")));
					p.addColumn(Bytes.toBytes("weather-data"), Bytes.toBytes("longitude"),
							Bytes.toBytes(locobj.getString("longitude")));
					p.addColumn(Bytes.toBytes("weather-data"), Bytes.toBytes("time"), Bytes.toBytes(times));

					table.put(p);

					System.out.println(p);
					i++;
				}

			});

		});
        // start the computation
		ssc.start();
		// wait for computation to terminate
		ssc.awaitTermination();

	}

}