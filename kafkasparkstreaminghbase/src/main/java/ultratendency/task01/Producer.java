package ultratendency.task01;

import java.util.Date;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class Producer {

	public static void main(String[] args) {

		// sample json data simulates the iot devices
		final String weather1 = "{'data': {'deviceId': '11c1310e-c0c2-461b-a4eb-f6bf8da2d23c','temperature':"
				+ (int) (Math.random() * ((40 - 10) + 1))
				+ ",'location': { 'latitude': '52.14691120000001','longitude': '11.658838699999933'},'time':"
				+ new Date().getTime() + "}}";

		final String weather2 = "{'data': {'deviceId': '21c1310e-c0c2-461b-a4eb-f6bf8da2d23c','temperature':"
				+ (int) (Math.random() * ((40 - 10) + 1))
				+ ",'location': { 'latitude': '62.14691120000001','longitude': '12.658838699999933'},'time':"
				+ new Date().getTime() + "}}";

		final String weather3 = "{'data': {'deviceId': '31c1310e-c0c2-461b-a4eb-f6bf8da2d23c','temperature':"
				+ (int) (Math.random() * ((40 - 10) + 1))
				+ ",'location': { 'latitude': '72.14691120000001','longitude': '13.658838699999933'},'time':"
				+ new Date().getTime() + "}}";

		Properties configProperties = new Properties();
		// defines list of brokers
		configProperties.put("bootstrap.servers", "localhost:9092");
		/*
		 * key is of type string, so key.serializer defines string serializer, when
		 * preparing the message for transmission to the broker.
		 */
		configProperties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		/*
		 * value is of type string, so value.serializer defines string serializer, when
		 * preparing the message for transmission to the broker.
		 */
		configProperties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		// metadata.broker.list defines where the producer can find one or moree
		// brokers.
		configProperties.put("metadata.broker.list", "localhost:9092");

		// The Kafka Producer class provides an option to connect a Kafka broker in its
		// constructor.
		final KafkaProducer<String, String> producer = new KafkaProducer<String, String>(configProperties);

		Runnable runnable = new Runnable() {

			public void run() {

				while (true) {
					// ProducerRecord − The producer manages a buffer of records waiting to be sent.
					ProducerRecord<String, String> rec1 = new ProducerRecord<String, String>("topic1", weather1);
					// KafkaProducer class provides send method to send messages asynchronously to a
					// topic.
					producer.send(rec1);

					System.out.println("producing messages and sending it to topic1");

					ProducerRecord<String, String> rec2 = new ProducerRecord<String, String>("topic2", weather2);
					// ProducerRecord<String,String>()
					producer.send(rec2);

					System.out.println("producing messages and sending it to topic2");

					ProducerRecord<String, String> rec3 = new ProducerRecord<String, String>("topic3", weather3);
					producer.send(rec3);

					System.out.println("producing messages and sending it to topic3");

					try {
						// for every second send a signal to kafka brokers.
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		};

		Thread thread = new Thread(runnable);
		thread.start();

		// producer.close();

	}

}
