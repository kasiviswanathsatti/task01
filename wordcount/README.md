# Hadoop Map Reduce : WordCount
---

A MapReduce job usually splits the input data-set into independent chunks, here the input data sets are splitted by 'space', which are processed by the map tasks in a completely parallel manner. The framework sorts the outputs of the maps, which are then input to the reduce tasks. Reduce task counts the number of occurance of each word and finally stores the output into FileSystem.   

---
## Prerequisites

Install Hadoop related to  your Operating system.

## how to run

---
### create a text file 'names.txt',add data(comma seperated) and put it into HDFS.
...
hadoop fs -put names.txt /user/cloudera/names.txt
...
### run the jar file
...
hadoop jar wordcount.jar WordCount names.txt wordcount
...
### check the output in HDFS
...
hadoop fs -cat /user/cloudera/output
...
---

## Running in a docker

---
### install docker
### pull the image
...
docker pull sequenceiq/hadoop-docker:2.7.0
...
### Start a container 
...
docker run -it sequenceiq/hadoop-docker:2.7.0 /etc/bootstrap.sh -bash
...
### Run MapReduce job.
...
cd $HADOOP_PREFIX
bin/hadoop dfs -put names.txt /user/root/input/
bin/hadoop jar wordcount.jar WordCount /user/root/input/names.txt /user/root/output
bin/hdfs -cat /user/root/output/*
...
---
