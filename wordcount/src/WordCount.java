import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class WordCount extends Configured implements Tool {

	// This program expects two arguments input path and output path
	// run method returns -1 if we not specify 2 arguments
	@Override
	public int run(String[] args) throws Exception {
		
		if (args.length < 2) {
			System.out.println("please give input and output directory properly");
			return -1;
		}
		
		/* JobConf is the primary interface for a user to describe a map-reduce job to the hadoop framework 
		 * for execution. The framework tries to faithfully execute the job as-is described by JobConf,
		 */
		JobConf conf = new JobConf(WordCount.class);

		// set the Path as the input for the map-reduce job.
		FileInputFormat.setInputPaths(conf, new Path(args[0]));
		// set the path as the output for the map-reduce job.
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));

		// set the mapper class for the job.
		conf.setMapperClass(WordMapper.class);
		// set the reducer class for the job.
		conf.setReducerClass(WordReducer.class);
		// set the key class for the map output data.
		conf.setMapOutputKeyClass(Text.class);
		// set the value class for the map output data.
		conf.setMapOutputValueClass(IntWritable.class);
		// set the key class for job output data.
		conf.setOutputKeyClass(Text.class);
		// set the value class for job outputs.
		conf.setOutputValueClass(IntWritable.class);
        /*
         * JobClient provides facilities to submit jobs, track their progress etc.
         * JobClient is the primary interface for the user-job to interact with the cluster.
         * runjob(), submits the job and returns only after the job has completed.
         */
		JobClient.runJob(conf);

		return 0;
	}

	public static void main(String args[]) throws Exception {
		//WordCount class extends configured,  
		//Tool is an extension of configurable interface provides run() method and is used with ToolRunner,
		//so it requires to implement Tool interface.
		//Tool Runner handles the common task of parsing out command line arguments and building the Configuration object
		
		int exitCode = ToolRunner.run(new WordCount(), args);
		System.out.println(exitCode);
	}
}
