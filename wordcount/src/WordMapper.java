import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

// Mapper class
public class WordMapper extends MapReduceBase implements 
Mapper<LongWritable, /*Input  key Type, input key to map function*/ 
Text,                /*Input  value Type, input value to map function */ 
Text,                /*Output key Type, output key of map function*/ 
IntWritable> {       /*Output value Type, output value of map function*/ 

	// Map funtion
	// OutputCollector collects the output of map function i.e intermediate output.
	// which is passed to reducer  by mapReduce framework.
	// Reporter report progress and update counters, status information etc.
	@Override
	public void map(LongWritable key, Text value,
			OutputCollector<Text, IntWritable> output, Reporter r)
			throws IOException {
		
		String s = value.toString();
		// split the data by space ,
		// output collector collects  (word , no of times it repeated)  
		for(String word:s.split(" ")){
			if(word.length()>0){
				output.collect(new Text(word), new IntWritable(1));
			}
		}
		
		
	}

}
