import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class WordReducer extends MapReduceBase implements
		  Reducer<Text  /* input key type, which is output key type of map function*/
	    , IntWritable   /* input value type, which is output value type of map function */
	    , Text          /* output key type of reduce funtion*/ 
	    , IntWritable>  /* output value type of reduce function*/{

	// reduce function
	// reduce function takes the intermediate output and counts the no of times the word occurs\
	//  OutputCollector collects the output of reduce function.
	@Override
	public void reduce(Text key, Iterator<IntWritable> values,
			OutputCollector<Text, IntWritable> output, Reporter r)
			throws IOException {

		int count = 0;
       // counts the no of times word occurs
		while (values.hasNext()) {
			IntWritable i = values.next();
			count += i.get();
		}

		output.collect(key, new IntWritable(count));

	}

}
