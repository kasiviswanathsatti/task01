package mapreducetest;

import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.junit.Test;

public class WordCountTest {
	HdfsTest path1 = new HdfsTest(new Path("/user/cloudera/kasi/names.txt"));
	HdfsTest path2 = new HdfsTest(new Path("/user/cloudera/kasi/namesspaceseperated.txt"));
	HdfsTest path3 = new HdfsTest(new Path("/user/cloudera/kasi/namesspaceseperated1.txt"));
	@Test
	public void wordCounttest1() throws IOException {
		assertEquals(9,path1.getCount("kasi"));
	}
	@Test
	public void wordCounttest2() throws IOException {
		assertEquals(2,path2.getCount("raju"));
	}	@Test
	public void wordCounttest3() throws IOException {
		assertEquals(9,path1.getCount("viswanath"));
	}	@Test
	public void wordCounttest4() throws IOException {
		assertEquals(61,path3.getCount("kasi"));
	}	@Test
	public void wordCounttest5() throws IOException {
		assertEquals(9,path1.getCount("satti"));
	}	@Test
	public void wordCounttest6() throws IOException {
		assertEquals(11,path2.getCount("kasi"));
	}	@Test
	public void wordCounttest7() throws IOException {
		assertEquals(9,path1.getCount("kishore"));
	}	@Test
	public void wordCounttest8() throws IOException {
		assertEquals(0,path2.getCount("satti"));
	}	@Test
	public void wordCounttest9() throws IOException {
		assertEquals(11,path3.getCount("naveen"));
	}	@Test
	public void wordCounttest10() throws IOException {
		assertEquals(0,path3.getCount("kasiviswanath"));
	}

}
