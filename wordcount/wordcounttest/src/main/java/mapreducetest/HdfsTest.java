package mapreducetest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class HdfsTest {
	public Path path;
	
	FileSystem fs;
	FSDataInputStream file;
	
	
	public HdfsTest(Path path) {
		this.path = path;
	}

	public FSDataInputStream configure() throws IOException {
       
		/*Configuration class passes the hadoop configuration information to FileSystem.
		 *  It loads the core-site  through class loader and keeps hadoop configuration information such as fs.defaultFS.*/
		Configuration conf = new Configuration();
		// we can set the configuration parameter
		conf.set("fs.defaultFS", "hdfs://localhost:8020");
		
		try {
			/*FileSystem uses Java IO FileSystem interface mainly DataInputStream and 
			 * DataOutputStream for IO operation.
			*/
			fs = FileSystem.get(conf);
			/* FileSystem have open() method which return FSDataInputStream,
			 * FSDataInputStream wraps the DataInputStream
             */
			file = fs.open(path);
		} catch (Exception e) {
			System.out.println("Null pointer exception caught");
			// how to handle null pointer exception
		}

		return file;
	}

	public int getCount(String name) throws IOException {
		int i = 0;
		String line = null;
		/*  An InputStreamReader is a bridge from byte streams to character streams.
		 *  It reads bytes and decodes them into characters.
		 *  BufferedReader is a wrapper for InputStreamReader,
		 *  reads a couple of characters from the specified stream and stores it in a buffer.  */
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				configure()));
		while ((line = reader.readLine()) != null) {
			String[] words = line.split(" ");
			for (String word : words) {
				if (word.equals(name)) {
					i += 1;
				}
			}
		}

		return i;
	}

	public static void main(String[] args) throws IOException {
		HdfsTest test = new HdfsTest(new Path("/user/cloudera/kasi/namesspaceseperated1.txt"));
		System.out.println(test.getCount("kasi"));
	}
}