import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class BinarySearchAlgorithmTest {

	int[] s = { 1, 2, 3, 7, 5, 6, 4, 11, 25, 78, 24, 18 };
	
	int[] s1 = { 5, 3, 6, 8, 9, 2, 4, 7, 12, 11, 98, 34 ,99};
	
	int[] s2 = { 4, 67, 89, 23, 45, 68, 12, 56, 98, 65, 87, 49 };

	@Test
	public void BinarySearchAlgorithmTestCase1() {
	    
		// The  given array is first sorted by BinarySearchAlgorithm.
		//  assertEquals compares the index value,output of binarysearch.
	  	
		assertEquals(1, BinarySearchAlgorithm.binarySearch(s, 2));

	}

	@Test
	public void BinarySearchAlgorithmTestCase2() {

		assertEquals(8, BinarySearchAlgorithm.binarySearch(s1, 11));
	}

	@Test
	public void BinarySearchAlgorithmTestCase3() {

		assertEquals(2, BinarySearchAlgorithm.binarySearch(s, 3));
	}

	@Test
	public void BinarySearchAlgorithmTestCase4() {

		assertEquals(6, BinarySearchAlgorithm.binarySearch(s1,8));
	}

	@Test
	public void BinarySearchAlgorithmTestCase5() {

		assertEquals(8, BinarySearchAlgorithm.binarySearch(s2, 68));
	}

	@Test
	public void BinarySearchAlgorithmTestCase6() {

		assertEquals(4, BinarySearchAlgorithm.binarySearch(s2, 49));
	}

	@Test
	public void BinarySearchAlgorithmTestCase7() {

		assertEquals(9, BinarySearchAlgorithm.binarySearch(s2, 87));
	}

	@Test
	public void BinarySearchAlgorithmTestCase8() {

		assertEquals(12, BinarySearchAlgorithm.binarySearch(s1, 99));
	}

}
