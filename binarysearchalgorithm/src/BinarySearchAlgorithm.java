import java.util.Arrays;

public class BinarySearchAlgorithm {

	/*key is the target item that we will search for in inputArray.
	binarySearch() will return true if it finds the key in the inputArray,
	and it will return false if the key is not in the list.*/
	
	public static int binarySearch(int[] inputArray, int key) {

		//beginning of the list
		int start = 0;
		// end of the list
		int end = inputArray.length - 1;
		// sorts the inputArray in ascending order 
		Arrays.sort(inputArray);
		
		
		while (start <= end) {
			// divide the list into two.
			int middle = (start + end) / 2;
			// if key is found in the middle of list return middle
			if (key == inputArray[middle]) {
				return middle;
			}
			/*If the item in the middle of the list is greater than our key, 
			 * we should look for the key in the bottom half of the list,
			 *  so we calculate a new value for end.
			 */
			if (key < inputArray[middle]) {
				end = middle - 1;
			}
			/*
			 * If the item in the middle of the list is less than our key,
			 * we should look for the key in the top half of the list, so we calculate a new value for start. 
			 */
			else {
				start = middle + 1;
			}
		}
		// if the key is not in the list return false
		return -1;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] s = { 1, 2, 3, 7, 5, 6, 4, 11, 25, 78, 24, 18 };
		int[] s1 = { 5, 3, 6, 8, 9, 2, 4, 7, 12, 11, 98, 34 ,99};
		int[] s2 = { 4, 67, 89, 23, 45, 68, 12, 56, 98, 65, 87, 49 };

		int key = 49;
		int a = binarySearch(s2, key);
		if (a != -1) {
			System.out.println("search key " + key + " found at index " + a);
		} else {
			System.out.println("searck key not found");
			// TODO Auto-generated method stub
		}

	}

}
